﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM.Setting.Abstraction.Model
{
    class analytics
    {
    }
    public class userAnalyticsUpdate
    {
        public string dashboardId { get; set; }
        public string host { get; set; }
        public string userId { get; set; }
        public string userMethod { get; set; }
    }
    public class userAnalyticsDelete
    {
        public string dashboardId { get; set; }
        public string host { get; set; }
        public string userId { get; set; }
        public string userMethod { get; set; }
        public string deleteId { get; set; }
        public string element { get; set; }

    }
}
