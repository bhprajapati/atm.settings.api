﻿using ATM.Setting.Abstraction.Base;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace ATM.Setting.Abstraction.Model
{
    public class UserEntity : BaseEntity
    {
        //[BsonId]
        //public ObjectId Id { get; set; }
        public string UserId { get; set; }
        public string Host { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string License { get; set; }
        public string Phone { get; set; }
        public string Extension { get; set; }
        public string Mobile { get; set; }
        public string Status { get; set; }
        //public BsonDateTime LastLogin { get; set; }
        //public BsonDateTime LastLogout { get; set; }
        public bool Active { get; set; }
        public string Post { get; set; }
    }
}
