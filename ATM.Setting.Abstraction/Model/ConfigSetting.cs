﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM.Setting.Abstraction.Model
{
    public class ConfigSetting
    {
        public string MongoDBConnectionString { get; set; }
        public string MongoDatabase { get; set; }
        public string PropertyCollectionName { get; set; }
        public string UserCollectionName { get; set; }
        public string RedisServer { get; set; }
        public string RedisConnectionString { get; set; }
        public string CachingAPIUrl { get; set; }
        public string RedisAPIUrl { get; set; }
        public string ReadCacheKeyAPIMethodUrl { get; set; }
        public string ContentType { get; set; }
        public string ReadSysPropertyAPIMethodUrl { get; set; }
        public string GetUserAnalyticsConfiguration { get; set; }
        public string AddUserAnalyticsConfiguration { get; set; }
        public string UpdateUserAnalyticsConfiguration { get; set; }
        public string DeleteUserAnalyticsConfiguration { get; set; }
        public string AddCacheKeyAPIMethodUrl { get; set; }
        public string UpdatePropertyKeyMethodUrl { get; set; }
        public string DeletePropertyModelMethodUrl { get; set; }
        public string AddSysPropertyMethodUrl { get; set; }
        public string GetCacheAllUrl { get; set; }
        public string PostSysPropertyUrl { get; set; }
        public string AddUserAPIMethodUrl { get; set; }
        public string UpdateUserAPIMethodUrl { get; set; }
        public string DeleteUserAPIMethodUrl { get; set; }
        public string GetUserByIdAPIMethodUrl { get; set; }
        public string PostSysPropertyAPIMethodUrl { get; set; }
        public string GetAllPropertiesByHostAPIMethodUrl { get; set; }
        public string ChangePasswordAPIMethodUrl { get; set; }
        public string AuthenticateAPIURL { get; set; }
        public bool IsDev { get; set; }
        public string GetUserByEmailAPIMethodUrl { get; set; }
        public string SaveSubscriberMethodUrl { get; set; }
        public string MongoAPIUrl { get; set; }
        public string GetSubscribersByHostUrl { get; set; }
    }
}
