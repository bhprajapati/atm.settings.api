﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM.Setting.Abstraction.Model
{
    public class SubscriberEntity
    {
        public string UserId { get; set; }
        public string Host { get; set; }
        public string PhoneNumber { get; set; }
    }
}
