﻿using ATM.Setting.Abstraction.Base;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;


namespace ATM.Setting.Abstraction.Model
{
    public class SysProperty:BaseEntity
    {
        public string HostPropId { get; set; }
        public string post { get; set; }
        public string PropId { get; set; }
        public string PropValue { get; set; }
        public string Description { get; set; }
        public string PropGroup { get; set; }
        public string Host { get; set; }
        public bool SysCaching { get; set; }
    }
}
