﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM.Setting.Abstraction.Model
{
    public class RedisKeyValuePair
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }
}
