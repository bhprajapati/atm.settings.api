﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM.Setting.Abstraction.ErrorMessage
{
    public class ErrorMessage
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
