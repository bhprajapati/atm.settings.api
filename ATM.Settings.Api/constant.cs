﻿namespace ATM.Settings.Api
{
    public static class Constant
    {
        public static class SettingsAPIConstant
        {
            public static string APICallStarted = "Execution of API Method {0} Started, {1}";
            public static string APICallEnded = "Execution of API Method {0} Ended, {1}";
            public static string APICallError = "Exception occured in API Method {0}, {1}";
        }
    }
}
