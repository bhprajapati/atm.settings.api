﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ATM.Setting.Abstraction.ErrorMessage;
using ATM.Setting.Abstraction.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Bson.IO;
using Newtonsoft.Json;
using static ATM.Settings.Api.Constant;

namespace ATM.Settings.Api.Controllers
{
    /// <summary>
    /// Handle DB operation for messages
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class MessageController : Controller
    {
        #region Private Variables
        private readonly ILogger _logger;
        private ConfigSetting _settings { get; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="configuration"></param>
        public MessageController(ILogger<SettingsController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _settings = configuration.GetSection("Service").Get<ConfigSetting>();
        }
        #endregion

        #region methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="subscriber"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("SaveSubscriber")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(List<SubscriberEntity>), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult SaveSubscriber([FromBody]List<SubscriberEntity> subscriber)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, subscriber));
            try
            {
                using (var client = new HttpClient())
                {
                    if (subscriber != null)
                    {

                        string apiUrl = _settings.MongoAPIUrl + _settings.SaveSubscriberMethodUrl;
                        client.BaseAddress = new Uri(apiUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                        StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(subscriber), Encoding.UTF8, _settings.ContentType);
                        var response = client.PostAsync(apiUrl, content).Result;
                        _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, subscriber));
                        if (response.IsSuccessStatusCode)
                        {
                            var err = new ErrorMessage();
                            err = Newtonsoft.Json.JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                            return StatusCode(200, new ErrorMessage { Status = "OK", Message = err.Message });
                        }
                        else
                        {
                            var err = new ErrorMessage();
                            err = Newtonsoft.Json.JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                            return StatusCode(500, new ErrorMessage { Status = "Error", Message = err.Message });
                        }
                    }
                    else
                    { return StatusCode(500, new ErrorMessage { Status = "Error", Message = " Null record error." }); }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hostName"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSubscribersByHost")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(List<SubscriberEntity>), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetSubscribersByHost(string hostName)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, hostName));
            var subscriberList = new List<SubscriberEntity>();

            try
            {
                string apiUrl = _settings.MongoAPIUrl + _settings.GetSubscribersByHostUrl + hostName;
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;
                        subscriberList = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SubscriberEntity>>(data);
                    }
                }
                _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, hostName));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(subscriberList);
        }
        #endregion
    }
}