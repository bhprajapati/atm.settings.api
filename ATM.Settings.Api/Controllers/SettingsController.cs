﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ATM.Setting.Abstraction.ErrorMessage;
using ATM.Setting.Abstraction.Model;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using static ATM.Settings.Api.Constant;

namespace ATM.Settings.Api.Controllers
{
    /// <summary>
    /// Settings Controller to handle CRUD operations for settings.
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class SettingsController : Controller
    {
        #region Private Variables
        private readonly ILogger _logger;
        private ConfigSetting _settings { get; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="configuration"></param>
        public SettingsController(ILogger<SettingsController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _settings = configuration.GetSection("Service").Get<ConfigSetting>();
        }
        #endregion

        /// <summary>
        /// Saves the property.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostSysProperty")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult PostSysProperty([FromBody]SysProperty model)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            try
            {
                using (var client = new HttpClient())
                {
                    if (model != null)
                    {
                        string apiUrl = "";
                        if (model.post == "add")
                            apiUrl = _settings.CachingAPIUrl + _settings.AddSysPropertyMethodUrl;
                        else if (model.post == "edit")
                            apiUrl = _settings.CachingAPIUrl + _settings.UpdatePropertyKeyMethodUrl;
                        if (apiUrl != "")
                        {
                            client.BaseAddress = new Uri(apiUrl);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                            StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, _settings.ContentType);
                            var response = client.PostAsync(apiUrl, content).Result;
                            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));
                            if (response.IsSuccessStatusCode)
                            {
                                return StatusCode(200, new ErrorMessage { Status = "OK", Message = "true" });
                            }
                            else
                            {
                                var err = new ErrorMessage();
                                err = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                                return StatusCode(200, new ErrorMessage { Status = "Error", Message = err.Message });
                            }
                        }
                        else
                            return StatusCode(500, new ErrorMessage { Status = "Error", Message = "Add/Edit Mode un-known." });
                    }
                    else
                    {
                        return StatusCode(200, new ErrorMessage { Status = "Error", Message = "Empty model insertion error." });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }

        }

        /// <summary>
        /// Deletes the property.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeletePropertyModel")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult DeletePropertyModel([FromBody]SysProperty model)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, string.Empty));

            try
            {
                if (model == null)
                {
                    return StatusCode(200, new ErrorMessage { Status = "Error", Message = "Empty Property Model" });
                }
                using (var client = new HttpClient())
                {
                    string apiUrl = _settings.CachingAPIUrl + _settings.DeletePropertyModelMethodUrl;
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                    StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, _settings.ContentType);
                    var response = client.PostAsync(apiUrl, content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var err = new ErrorMessage();
                        err = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                        return StatusCode(200, new ErrorMessage { Status = "success", Message = err.Message });
                    }
                }
                return StatusCode(200, new ErrorMessage { Status = "sucess", Message = "Success deleted the record" });
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, string.Empty));

        }

        /// <summary>
        /// Returns the property for provided host property id.
        /// </summary>
        /// <param name="hostPropId"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetSysPropertyById")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetSysPropertyById(string hostPropId)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, hostPropId));
            var retVal = new RedisKeyValuePair();

            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = _settings.CachingAPIUrl + _settings.ReadSysPropertyAPIMethodUrl;
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                    StringContent content = new StringContent(JsonConvert.SerializeObject(hostPropId), Encoding.UTF8, _settings.ContentType);
                    var response = client.GetAsync(apiUrl + hostPropId).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        retVal = JsonConvert.DeserializeObject<RedisKeyValuePair>(response.Content.ReadAsStringAsync().Result);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, hostPropId));
            return Json(retVal);
        }

        /// <summary>
        /// Gets all cacheable properties for given host
        /// </summary>
        /// <param name="host"></param>
        /// <returns>List of Key Value Pair</returns>
        [HttpGet]
        [Route("GetCacheAll")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetCacheAll(string host)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, host));
            var retVal = new List<RedisKeyValuePair>();
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = _settings.CachingAPIUrl + _settings.GetCacheAllUrl + host;
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                    StringContent content = new StringContent(JsonConvert.SerializeObject(host), Encoding.UTF8, _settings.ContentType);
                    var response = client.GetAsync(apiUrl).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;
                        retVal = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RedisKeyValuePair>>(data);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host));
            return Json(retVal);
        }

        /// <summary>
        /// Addes a property in redis in background.
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddPostSysProperty")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult AddPostSysProperty([FromBody]SysProperty model)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            try
            {
                using (var client = new HttpClient())
                {
                    if (model != null)
                    {
                        string apiUrl = _settings.CachingAPIUrl + _settings.PostSysPropertyAPIMethodUrl;
                        client.BaseAddress = new Uri(apiUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                        StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, _settings.ContentType);
                        var response = client.PostAsync(apiUrl, content).Result;
                        _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));
                        if (response.IsSuccessStatusCode)
                        {
                            var err = new ErrorMessage();
                            err = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                            return StatusCode(200, new ErrorMessage { Status = "OK", Message = err.Message });
                        }
                        else
                        {
                            var err = new ErrorMessage();
                            err = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                            return StatusCode(200, new ErrorMessage { Status = "Error", Message = err.Message });
                        }
                    }
                    else
                    { return StatusCode(200, new ErrorMessage { Status = "Error", Message = " Null record error." }); }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Add/Edit User details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("PostUserDetails")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(UserEntity), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult PostUserDetails([FromBody]UserEntity model)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            try
            {
                using (var client = new HttpClient())
                {
                    if (model != null)
                    {
                        string apiUrl = "";
                        if (model.Post == "add")
                            apiUrl = _settings.CachingAPIUrl + _settings.AddUserAPIMethodUrl;
                        else if (model.Post == "edit")
                            apiUrl = _settings.CachingAPIUrl + _settings.UpdateUserAPIMethodUrl;
                        if (apiUrl != "")
                        {
                            client.BaseAddress = new Uri(apiUrl);
                            client.DefaultRequestHeaders.Accept.Clear();
                            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                            StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, _settings.ContentType);
                            var response = client.PostAsync(apiUrl, content).Result;
                            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));
                            if (response.IsSuccessStatusCode)
                            {
                                return StatusCode(200, new ErrorMessage { Status = "OK", Message = "true" });
                            }
                            else
                            {
                                var err = new ErrorMessage();
                                err = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                                return StatusCode(200, new ErrorMessage { Status = "OK", Message = err.Message });
                            }
                        }
                        else
                            return StatusCode(500, new ErrorMessage { Status = "Error", Message = "Add/Edit Mode un-known." });
                    }
                    else
                    {
                        return StatusCode(200, new ErrorMessage { Status = "Error", Message = "Model is empty." });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Delete User details
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("DeleteUser")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(UserEntity), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult DeleteUser([FromBody]UserEntity model)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, string.Empty));
            try
            {
                using (var client = new HttpClient())
                {
                    if (model != null)
                    {
                        string apiUrl = _settings.CachingAPIUrl + _settings.DeleteUserAPIMethodUrl;
                        client.BaseAddress = new Uri(apiUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                        StringContent content = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(model), UnicodeEncoding.UTF8, "application/json");
                        var response = client.PostAsync(apiUrl, content).Result;
                        _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, string.Empty));

                        if (response.IsSuccessStatusCode)
                        {
                            return StatusCode(200, new ErrorMessage { Status = "OK", Message = "true" });
                        }
                        else
                        {
                            var err = new ErrorMessage();
                            err = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                            return StatusCode(500, new ErrorMessage { Status = "Error", Message = err.Message });
                        }
                    }
                    else
                    {
                        return StatusCode(200, new ErrorMessage { Status = "Error", Message = "Model is Empty" });
                    }
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Gets UserDetails by provided userId and host
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="host"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUserById")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(UserEntity), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetUserById(string userId, string host)
        {
            var urlString = "&host=";
            var userProperties = new UserEntity();
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, userId));
            try
            {
                string apiUrl = _settings.CachingAPIUrl + _settings.GetUserByIdAPIMethodUrl + userId + urlString + host;

                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;
                        userProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<UserEntity>(data);
                    }
                }
                _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, userId));

            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, userId));
            return Json(userProperties);
        }

        /// <summary>
        /// Gets UserDetails by provided userId and host
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetUserByEmail")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(UserEntity), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetUserByemail(string email)
        {
            //var urlString = "&password=";
            var userProperties = new UserEntity();
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, email));
            try
            {
                string apiUrl = _settings.CachingAPIUrl + _settings.GetUserByEmailAPIMethodUrl + email;

                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;
                        userProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<UserEntity>(data);
                    }
                }
                _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, email));

            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, email));
            return Json(userProperties);
        }

        /// <summary>
        /// Gets All Properties by provided host
        /// </summary>
        /// <param name="host"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAllPropertiesByHost")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(IList<Setting.Abstraction.Model.RedisKeyValuePair>), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JsonResult GetAllPropertiesByHost(string host)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, host));
            var sysProperties = new List<Setting.Abstraction.Model.RedisKeyValuePair>();
            try
            {
                string apiUrl = _settings.CachingAPIUrl + _settings.GetAllPropertiesByHostAPIMethodUrl + host;
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                    HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        var data = response.Content.ReadAsStringAsync().Result;
                        sysProperties = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RedisKeyValuePair>>(data);
                    }
                }
                _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host));
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            return Json(sysProperties);
        }

        /// <summary>
        /// Change password
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("ChangePassword")]
        [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(UserEntity), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public ObjectResult ChangePassword([FromBody]UserEntity model)
        {
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, model));
            try
            {
                using (var client = new HttpClient())
                {
                    if (model != null)
                    {
                        string apiUrl = _settings.CachingAPIUrl + _settings.ChangePasswordAPIMethodUrl;
                        client.BaseAddress = new Uri(apiUrl);
                        client.DefaultRequestHeaders.Accept.Clear();
                        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                        StringContent content = new StringContent(JsonConvert.SerializeObject(model), Encoding.UTF8, _settings.ContentType);
                        var response = client.PostAsync(apiUrl, content).Result;
                        _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, model));
                        if (response.IsSuccessStatusCode)
                        {
                            var err = new ErrorMessage();
                            err = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                            return StatusCode(200, new ErrorMessage { Status = "OK", Message = err.Message });
                        }
                        else
                        {
                            var err = new ErrorMessage();
                            err = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                            return StatusCode(200, new ErrorMessage { Status = "Error", Message = err.Message });
                        }
                    }
                    else
                    {
                        return StatusCode(200, new ErrorMessage { Status = "Error", Message = " Null record error." });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(String.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }
    }
}
