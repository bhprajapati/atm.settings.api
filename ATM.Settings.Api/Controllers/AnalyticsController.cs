﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using Newtonsoft.Json;
using static ATM.Settings.Api.Constant;
using ATM.Setting.Abstraction.ErrorMessage;
using ATM.Setting.Abstraction.Model;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Reflection;
using System.Net.Http;
using System;
using System.Net.Http.Headers;
using System.Text;
using Newtonsoft.Json.Linq;

namespace ATM.Settings.Api.Controllers
{
    /// <summary>
    /// SettingsAnalytics Controller to handle CRUD operations for Analytics.
    /// </summary>
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class SettingsAnalyticsController : Controller
    {
        #region Private Variables
        private readonly ILogger _logger;
        private ConfigSetting _settings { get; }
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="configuration"></param>
        public SettingsAnalyticsController(ILogger<SettingsController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _settings = configuration.GetSection("Service").Get<ConfigSetting>();
        }
        #endregion

        /// <summary>
        /// Returns the Analytics Configuration .
        /// </summary>
        /// <param name="host">Domain</param>
        /// <param name="userId">User ID who is logged in</param>
        /// <returns></returns>
        [HttpGet]
        [Route("GetAnalyticsConfiguration")]
      //  [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JObject GetUserAnalyticsConfiguration(string host,string userId)
        {
            _logger.LogInformation(System.String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, host + " - "+ userId));
            JObject retVal = null;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = _settings.CachingAPIUrl + _settings.GetUserAnalyticsConfiguration;
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));

                    StringContent content = new StringContent(JsonConvert.SerializeObject(host), Encoding.UTF8, _settings.ContentType);
                    var response = client.PostAsync(apiUrl + host + "&userId=" + userId,content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        retVal = JObject.Parse(response.Content.ReadAsStringAsync().Result);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
            }
            _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, host + " - " + userId));
            return retVal;
        }

        /// <summary>
        /// Adds the User Analytics Configuration .
        /// </summary>
        /// <param name="usrConfig">User configuration</param>
        /// <returns></returns>
        [HttpPost]
        [Route("AddUserAnalyticsConfiguration")]
        //  [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JObject addUserAnalyticsConfiguration([FromBody]userAnalyticsUpdate usrConfig)
        {
            _logger.LogInformation(System.String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, usrConfig.host + " - " + usrConfig.userId));
            JObject globalConfig = null;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = _settings.CachingAPIUrl + _settings.AddUserAnalyticsConfiguration;
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                    StringContent content = new StringContent(JsonConvert.SerializeObject(usrConfig), Encoding.UTF8, _settings.ContentType);
                    var response = client.PostAsync(apiUrl, content).Result;
                    _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, usrConfig.host + " - " + usrConfig.userId));
                    if (response.IsSuccessStatusCode)
                    {
                        globalConfig = JsonConvert.DeserializeObject<JObject>(response.Content.ReadAsStringAsync().Result.ToString());
                        return globalConfig;
                    }
                    else
                    {
                        //var err = new ErrorMessage();
                        globalConfig = JsonConvert.DeserializeObject<JObject>(response.Content.ReadAsStringAsync().Result.ToString());
                        return globalConfig;
                        //return StatusCode(500, new ErrorMessage { Status = "Error", Message = err.Message });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));

                string errorMessage = "{ Status:'Error', Message:'" + ex.Message + "'}";
                return globalConfig = JObject.Parse(errorMessage);
                //return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Updates the User Analytics Configuration .
        /// </summary>
        /// <param name="usrConfig">User configuration</param>
        /// <returns></returns>
        [HttpPost]
        [Route("updateUserAnalyticsConfiguration")]
        //  [Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JObject updateUserAnalyticsConfiguration([FromBody]userAnalyticsUpdate usrConfig)
        {
            _logger.LogInformation(System.String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, usrConfig.host + " - " + usrConfig.userId));
            JObject globalConfig = null;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = _settings.CachingAPIUrl + _settings.UpdateUserAnalyticsConfiguration;
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                    StringContent content = new StringContent(JsonConvert.SerializeObject(usrConfig), Encoding.UTF8, _settings.ContentType);
                    var response = client.PostAsync(apiUrl,content).Result;

                    _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, usrConfig.host + " - " + usrConfig.userId));
                    if (response.IsSuccessStatusCode)
                    {
                        //var err = new ErrorMessage();
                        globalConfig = JsonConvert.DeserializeObject<JObject>(response.Content.ReadAsStringAsync().Result.ToString());
                        return globalConfig;
                        //return StatusCode(200, new ErrorMessage { Status = "OK", Message = err.Message });
                    }
                    else
                    {
                        //var err = new ErrorMessage();
                        globalConfig = JsonConvert.DeserializeObject<JObject>(response.Content.ReadAsStringAsync().Result.ToString());
                        return globalConfig;
                        //return StatusCode(500, new ErrorMessage { Status = "Error", Message = err.Message });
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                string errMessage = "{Status:'Error',Message:'" + ex.Message + "'}";
                return globalConfig = JObject.Parse(errMessage);

                //return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
            }
        }

        /// <summary>
        /// Delete the User Analytics Configuration Layout/Preset.
        /// </summary>
        /// <param name="usrConfig">User configuration</param>
        /// <returns></returns>
        [HttpPost]
        [Route("deleteUserAnalyticsConfiguration")]
        //[Attributes.ATMAuthorize()]
        [ProducesResponseType(typeof(SysProperty), 200)]
        [ProducesResponseType(typeof(ErrorMessage), 500)]
        public JObject deleteUserAnalyticsConfiguration([FromBody]userAnalyticsDelete usrConfig)
        {
            _logger.LogInformation(System.String.Format(SettingsAPIConstant.APICallStarted, MethodBase.GetCurrentMethod().Name, usrConfig.host + " - " + usrConfig.userId));
            JObject globalConfig = null;
            try
            {
                using (var client = new HttpClient())
                {
                    string apiUrl = _settings.CachingAPIUrl + _settings.DeleteUserAnalyticsConfiguration;
                    client.BaseAddress = new Uri(apiUrl);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(_settings.ContentType));
                    StringContent content = new StringContent(JsonConvert.SerializeObject(usrConfig), Encoding.UTF8, _settings.ContentType);
                    var response = client.PostAsync(apiUrl, content).Result;
                    _logger.LogInformation(String.Format(SettingsAPIConstant.APICallEnded, MethodBase.GetCurrentMethod().Name, usrConfig.host + " - " + usrConfig.userId));
                    if (response.IsSuccessStatusCode)
                    {
                        //var err = new ErrorMessage();
                        globalConfig = JsonConvert.DeserializeObject<JObject>(response.Content.ReadAsStringAsync().Result.ToString());
                        return globalConfig;

                    }
                    else
                    {
                        //var err = new ErrorMessage();
                        globalConfig = JsonConvert.DeserializeObject<JObject>(response.Content.ReadAsStringAsync().Result.ToString());
                        return globalConfig;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(string.Format(SettingsAPIConstant.APICallError, MethodBase.GetCurrentMethod().Name, ex));
                //return StatusCode(500, new ErrorMessage { Status = "Error", Message = ex.Message });
                string errMessage = "{Status:'Error', Message:'" + ex.Message + "'}";
                return globalConfig = JObject.Parse(errMessage);
            }
        }
    }
}