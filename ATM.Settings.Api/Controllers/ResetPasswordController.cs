﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoApi.Interface;
using MongoApi.Models;

namespace ATM.Settings.Api.Controllers
{
    [Route("api/[controller]")]
    [EnableCors("AllowCors")]
    public class ResetPasswordController : Controller
    {
        private IResetPasswordRepository _resetPasswordRepository;

        public ResetPasswordController(IResetPasswordRepository resetPasswordRepository)
        {
            _resetPasswordRepository = resetPasswordRepository;
        }
        [HttpGet]
        [Route("GetPasswords")]
        public IEnumerable<ChangePasswordEntity> GetPasswords()
        {
            return _resetPasswordRepository.GetPasswords();
        }
        [HttpPost]
        [Route("ResetPassword")]
        public bool ResetPassword(ChangePasswordEntity model)
        {
            return _resetPasswordRepository.ResetPassword(model);
        }
    }
}
