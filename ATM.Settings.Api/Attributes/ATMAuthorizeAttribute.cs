﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using ATM.Setting.Abstraction.Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace ATM.Settings.Api.Attributes
{  
    /// <summary>
    /// Authorization Attribute
    /// </summary>
    public class ATMAuthorizeAttribute : TypeFilterAttribute
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public ATMAuthorizeAttribute() : base(typeof(ATMAuthorizeAttributeExecutor))
        {

        }

        private class ATMAuthorizeAttributeExecutor : Attribute, IAsyncResourceFilter
        {
            private ConfigSetting settings;
            public ATMAuthorizeAttributeExecutor()
            {
            }
            public async Task OnResourceExecutionAsync(ResourceExecutingContext context, ResourceExecutionDelegate next)
            {
                var configuration = context.HttpContext.RequestServices.GetService<IConfiguration>();
                settings = configuration.GetSection("Service").Get<ConfigSetting>();

                var isAuthorized = IsValidUser(context.HttpContext.Request);
                if (!isAuthorized)
                {
                    context.Result = new UnauthorizedResult();
                    await context.Result.ExecuteResultAsync(context);
                }
                else
                {
                    await next();
                }
            }

            private bool IsValidUser(Microsoft.AspNetCore.Http.HttpRequest request)
            {
                if (settings.IsDev)
                {
                    return true;
                }
                bool isAuthorized = false;
                //Token Authorization
                //Authorization  
                string authToken = request.Headers["Authorization"];

                if (string.IsNullOrEmpty(authToken)) return isAuthorized;
                try
                {
                    using (var authClient = new HttpClient())
                    {
                        authClient.BaseAddress = new Uri(settings.AuthenticateAPIURL);
                        authClient.DefaultRequestHeaders.Accept.Clear();
                        authClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(settings.ContentType));
                        authClient.DefaultRequestHeaders.Add("Authorization", authToken);
                        var response = authClient.GetAsync(settings.AuthenticateAPIURL).Result;
                        if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
                        {
                            //return Json("Not Authorized");
                            return isAuthorized;
                        }
                        if (response.IsSuccessStatusCode)
                        {
                            string authResponse = response.Content.ReadAsStringAsync().Result;
                            isAuthorized = true;
                        }
                    }
                }
                catch (Exception)
                {
                    return isAuthorized;
                }
                return isAuthorized;
            }

        }
    }
}
