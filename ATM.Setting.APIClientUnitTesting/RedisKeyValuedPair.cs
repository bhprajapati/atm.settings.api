﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATM.Setting.APIClientUnitTesting
{
    public class RedisKeyValuedPair
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class AuthTokenResponse
    {
        public string access_token { get; set; }
        public int expires_in { get; set; }
    }

}
