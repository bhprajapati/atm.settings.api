﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Setting.APIClientUnitTesting
{
    public class SysProperty
    {
        public string HostPropId { get; set; }
        public string PropId { get; set; }
        public string PropValue { get; set; }
        public string Description { get; set; }
        public string PropGroup { get; set; }
        public string Host { get; set; }
        public bool SysCaching { get; set; }
        public string post { get; set; }
        public DateTime DateCreated { get; set; }
        public long CreatedBy { get; set; }
        public DateTime DateUpdated { get; set; }
        public long UpdatedBy { get; set; }
    }
}
