﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace ATM.Setting.APIClientUnitTesting
{
    [TestClass]
    public class SettingManagerUnitTesting
    {
        string urlEndPoint = ConfigurationManager.AppSettings["ATMSettingURL"].ToString();
        string authTokenUrl = ConfigurationManager.AppSettings["AuthTokenAPIMethodURL"].ToString();
        SysProperty sysProperty_Update = null;
        SysProperty sysProperty_Save = null;
        SysProperty sysProperty_Null = null;
        SysProperty sysProperty_Delete = null;

        UserEntity userEntity_Save = null;
        UserEntity userEntity_Null = null;
        UserEntity userEntity_Delete = null;
        UserEntity userEntity_Update = null;
        UserEntity userEntity_ChangeUserPassword = null;
        AuthTokenResponse authtoken = null;

        public SettingManagerUnitTesting()
        {
            sysProperty_Save = new SysProperty { post = "add", PropId = "List-Redis22", SysCaching = true, PropValue = "RedisKey-2", Description = "Data is sending from caching API", PropGroup = "RedisKeyValue", Host = "PeerBrains", CreatedBy = 104 };
            sysProperty_Update = new SysProperty { post = "edit", HostPropId = "PeerBrains:List-Redis22", PropValue = "10", Description = "For the Host PeerBrains", UpdatedBy = 103, SysCaching = true };
            sysProperty_Delete = new SysProperty { HostPropId = "PeerBrains:List-Redis22", SysCaching = true };
            userEntity_Save = new UserEntity { UserId = "Test01", Host = "Peerbrains.com", FirstName = "Test", LastName = "ABC", Mobile = "8546321475", Email = "Test@gmail.com", post = "add", License = "HR", Active = true, Password = "Test01@123", Extension = "012", CreatedBy = 101 };
            userEntity_Delete = new UserEntity { UserId = "Test01", Host = "Peerbrains.com", UpdatedBy = 102 };
            userEntity_Update = new UserEntity { UserId = "Test01", Host = "Peerbrains.com", Mobile = "7845123691", Email = "test@hotmail.com", post = "edit", License = "HM", UpdatedBy = 104 };
            userEntity_ChangeUserPassword = new UserEntity { UserId = "Test01", Host = "Peerbrains.com", Password = "test@hot123", UpdatedBy = 102 };
            GetAuthToken(userEntity_Save.UserId, userEntity_Save.Password, userEntity_Save.Host);
        }


        [TestMethod]
        public void GetSysPropertyById()
        {
            string keyForMongo = "GetSysPropertyById";
            string parameter = "?hostPropId=PeerBrains:List-Redis22";
            string apiUrl = urlEndPoint + keyForMongo + parameter;
            Trace.WriteLine("The Request Object is :" + "");

            //Token Authorization            
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<SysProperty>(data);
                    Trace.WriteLine("The Object Of hostPropId of " + parameter + " is :" + data.ToString());
                }
            }

        }
        [TestMethod]
        public void GetSysPropertyById_WhenHostPropIdIsEmpty()
        {


            string keyForMongo = "GetSysPropertyById";
            string parameter = "?hostPropId= ";
            string apiUrl = urlEndPoint + keyForMongo + parameter;
            Trace.WriteLine("The Request Object For Method :" + keyForMongo);
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<SysProperty>(data);
                    var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                    Trace.WriteLine("The Object Of hostPropId of " + parameter + " is :" + erm.Message);
                }
            }
        }

        [TestMethod]
        public void AddSysProperty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Save));
            System.Net.HttpStatusCode result;
            string keyForMongo = "PostSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Save), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void AddSysProperty_WhenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Null));
            System.Net.HttpStatusCode result;
            string keyForMongo = "PostSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Null), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void UpdatePropertyKey()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Update));
            System.Net.HttpStatusCode result;
            string keyForMongo = "PostSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Update), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void UpdatePropertyKey_WhenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Null));
            System.Net.HttpStatusCode result;
            string keyForMongo = "PostSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Null), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void DeletePropertyKey()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject("PeerBrains:List-Redis2"));
            System.Net.HttpStatusCode result;
            string keyForMongo = "DeletePropertyModel";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Delete), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void DeletePropertyKey_whenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Null));
            System.Net.HttpStatusCode result;
            string keyForMongo = "DeletePropertyModel";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Null), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void GetCacheAll()
        {

            Trace.WriteLine("The Request Object is :" + "");
            string keyForMongo = "GetCacheAll";
            string domain = "?host=PeerBrains";
            string apiUrl = urlEndPoint + keyForMongo + domain;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RedisKeyValuedPair>>(data);
                    Trace.WriteLine("The Object Of hostPropId of " + domain + " is :" + data);
                }
            }
        }
        [TestMethod]
        public void GetCacheAll_WhenDomainIsEmpty()
        {

            Trace.WriteLine("The Request Object is :" + "");
            string keyForMongo = "GetCacheAll";
            string domain = "?domain=";
            string apiUrl = urlEndPoint + keyForMongo + domain;

            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RedisKeyValuedPair>>(data);
                    Trace.WriteLine("The Object Of hostPropId of " + domain + " Message is :" + data.ToString());
                }
            }
        }
        [TestMethod]
        public void PostSysProperty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Save));
            System.Net.HttpStatusCode result;
            string keyForMongo = "AddPostSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Save), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void PostSysProperty_WhenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Null));
            System.Net.HttpStatusCode result;
            string keyForMongo = "AddPostSysProperty";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(sysProperty_Null), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void AddUserEntity()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Save));
            System.Net.HttpStatusCode result;
            string keyForMongo = "PostUserDetails";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Save), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void AddUserEntity_WhenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Null));
            System.Net.HttpStatusCode result;
            string keyForMongo = "PostUserDetails";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Null), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void UpdateUserEntity()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Update));
            System.Net.HttpStatusCode result;
            string keyForMongo = "PostUserDetails";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Update), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void UpdateUserEntity_WhenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Null));
            System.Net.HttpStatusCode result;
            string keyForMongo = "PostUserDetails";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Null), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void DeleteUserEntity()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Delete));
            System.Net.HttpStatusCode result;
            string keyForMongo = "DeleteUser";
            string apiUrl = urlEndPoint + keyForMongo;

            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Delete), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void DeleteUserEntity_WhenModelIsEmpty()
        {
            Trace.WriteLine("The Request Object is :" + Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Null));
            System.Net.HttpStatusCode result;
            string keyForMongo = "DeleteUser";
            string apiUrl = urlEndPoint + keyForMongo;
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Null), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void ChangeUserPassword()
        {
            System.Net.HttpStatusCode result;
            string keyForMongo = "ChangePassword";
            string apiUrl = urlEndPoint + keyForMongo;
            Trace.WriteLine("The Request Object for method :" + keyForMongo);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_ChangeUserPassword), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }
        [TestMethod]
        public void ChangeUserPassword_WhenModelIsEmpty()
        {

            System.Net.HttpStatusCode result;
            string keyForMongo = "ChangePassword";
            string apiUrl = urlEndPoint + keyForMongo;
            Trace.WriteLine("The Request Object for method :" + keyForMongo);
            using (HttpClient client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                StringContent postData = new StringContent(Newtonsoft.Json.JsonConvert.SerializeObject(userEntity_Null), UnicodeEncoding.UTF8, "application/json");
                var response = client.PostAsync(apiUrl, postData).Result;
                result = response.StatusCode;
                var erm = JsonConvert.DeserializeObject<ErrorMessage>(response.Content.ReadAsStringAsync().Result.ToString());
                Trace.WriteLine("The Response Of the Method is :" + erm.Message);
            }
            Assert.IsTrue(result == HttpStatusCode.OK);
        }

        [TestMethod]
        public void GetUserById()
        {
            string keyForMongo = "GetUserById";
            string parameter = "?userId=Test01&host=Peerbrains.com";
            string apiUrl = urlEndPoint + keyForMongo + parameter;
            Trace.WriteLine("The Request User Object of userId and host is :" + parameter);
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<UserEntity>(data);
                    Trace.WriteLine("The Object Of hostPropId of " + parameter + " is :" + data.ToString());
                }
            }
        }
        [TestMethod]
        public void GetUserById_WhenUserAndHostIsEmpty()
        {


            string keyForMongo = "GetUserById";
            string parameter = "?userId=&host=";
            string apiUrl = urlEndPoint + keyForMongo + parameter;
            Trace.WriteLine("The Request Object For Method:" + keyForMongo);
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<UserEntity>(data);
                    Trace.WriteLine("The Object Of hostPropId of " + parameter + " is :" + data.ToString());
                }
            }
        }

        [TestMethod]
        public void GetUserByEmailId()
        {
            string keyForMongo = "GetUserByEmail";
            string parameter = "?email=Raj04@gmail.com";
            string apiUrl = urlEndPoint + keyForMongo + parameter;
            Trace.WriteLine("The Request User Object of userId and host is :" + parameter);
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<UserEntity>(data);
                    Trace.WriteLine("The Object Of EmailId of " + parameter + " is :" + data.ToString());
                }
            }
        }

        [TestMethod]
        public void GetAllPropertiesByHost()
        {


            string keyForMongo = "GetAllPropertiesByHost";
            string parameter = "?host=PeerBrains";
            string apiUrl = urlEndPoint + keyForMongo + parameter;
            Trace.WriteLine("The Request Object for method:" + keyForMongo);
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RedisKeyValuedPair>>(data);
                    Trace.WriteLine("The Object Of hostPropId of " + parameter + " is :" + data.ToString());
                }
            }
        }
        [TestMethod]
        public void GetAllPropertiesByHost_WhenHostIsEmpty()
        {
            string keyForMongo = "GetAllPropertiesByHost";
            string parameter = "?hostName=";
            string apiUrl = urlEndPoint + keyForMongo + parameter;
            Trace.WriteLine("The Request Object for method:" + keyForMongo);
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(apiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + authtoken.access_token);
                HttpResponseMessage response = client.GetAsync(apiUrl).Result;
                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    var responseData = Newtonsoft.Json.JsonConvert.DeserializeObject<List<RedisKeyValuedPair>>(data);
                    Trace.WriteLine("The Object Of hostPropId of " + parameter + " is :" + data.ToString());
                }
            }
        }

        private void GetAuthToken(string username, string password, string host)
        {
            //Token Authorization
            using (var authClient = new HttpClient())
            {
                string authApiUrl = authTokenUrl;
                // Here you create your parameters to be added to the request content
                string body = "username=" + username + "&password=" + password + "&host=" + host;
                StringContent postData = new StringContent(body, Encoding.UTF8,
                                            "application/x-www-form-urlencoded");
                var response = authClient.PostAsync(authApiUrl, postData).Result;

                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    authtoken = Newtonsoft.Json.JsonConvert.DeserializeObject<AuthTokenResponse>(data);
                }
            }
        }
    }
}
