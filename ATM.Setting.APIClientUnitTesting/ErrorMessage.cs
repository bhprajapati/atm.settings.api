﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ATM.Setting.APIClientUnitTesting
{
    public class ErrorMessage
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
